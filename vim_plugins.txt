https://github.com/tpope/vim-fugitive.git
https://github.com/scrooloose/nerdcommenter.git
https://github.com/scrooloose/nerdtree.git
https://github.com/sheerun/vim-polyglot.git
https://github.com/mhinz/vim-grepper.git
https://github.com/dense-analysis/ale.git
https://github.com/sjl/badwolf.git
