execute pathogen#infect()
syntax on
filetype plugin indent on

set relativenumber
set nocompatible
set encoding=utf-8
set ruler
set colorcolumn=100
set incsearch
set smartcase
set smartindent
set cinkeys-=0#
set hlsearch
set linebreak
set pastetoggle=<F2>
set noswapfile
set nobackup
set autoread
set hidden
set wildmenu
set wildmode=list:longest,full
set wildignore=*.pyc
set list
set lcs=tab:▸\ ,trail:·,eol:¬,nbsp:_
set backspace=2 " make backspace work like most other apps
set laststatus=2

map <leader>] :bnext<CR>
map <leader>[ :bprev<CR>
nmap <leader>e :e#<CR>
map <leader>x :bdelete<CR>
nmap <leader>h :nohlsearch<CR>
map <leader>n :NERDTreeToggle<CR>
map j gj
map k gk

" I always work on 4 or 2 spaces indent files :)
set autoindent
set expandtab
set smarttab
set tabstop=2
set shiftwidth=2
command! Q q
command! W w

" Colors
if has("termguicolors")
  set termguicolors
endif
set t_Co=256
set background=dark

colorscheme badwolf
